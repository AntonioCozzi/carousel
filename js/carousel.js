const CHUNK_SIZE = 10;

class Carousel {
    constructor(options) {
        this.options = options;

        this.init()
    }

    // generates the template.
    // first function to be executed.
    init() {
        // get the body
        let body = document.querySelector("body");

        // generate the main container for the carousel
        let parentContainer = document.createElement("div");
        // setting the id of the node to options.container
        parentContainer.id = this.options.container;

        // generates the head node and gives to it a template
        let carouselHead = document.createElement("div");
        carouselHead.classList.add("carousel-head");
        carouselHead.innerHTML = `
            <div class="icon-container">
                <i class="fas fa-lightbulb"></i>
            </div>
            <div class="title-container">
                <p class="title">${this.options.title} <i class="fas fa-chevron-right"></i></p>
                <p class="subtitle">${this.options.subtitle}</p>
            </div>`;

        // generates the carousel body
        let carouselBody = document.createElement("ul");
        carouselBody.classList.add("carousel-body");

        // fetches the cards from the fake REST API call. 
        // uses promises chaining.
        let cards = this.options.fetchCards(CHUNK_SIZE);

        // generates the indicator
        let indicator = document.createElement("div")
        indicator.innerHTML = `
            <div id="indicator">
                <i id="indicator-icon" class="fas fa-chevron-right"></i>
            </div>
        `
        carouselBody.prepend(indicator)

        let that = this;

        document.addEventListener('click', function (e) {
            if (e.target && e.target.id == 'indicator-icon') {
                that.options.fetchCards(CHUNK_SIZE).then(fetchedCards => {
                    parentContainer.append(carouselBody);

                    let cardsNodes = that.generateCard(fetchedCards)
                    cardsNodes.forEach(cardNode => {
                        // appends each generated card to the carousel body
                        carouselBody.prepend(cardNode)
                    });
                })
            }
        });

        cards.then(fetchedCards => {
            parentContainer.append(carouselBody);

            let cardsNodes = this.generateCard(fetchedCards)
            cardsNodes.forEach(cardNode => {
                // appends each generated card to the carousel body
                carouselBody.prepend(cardNode)
            });
        })

        // appends the head to the parent container
        parentContainer.append(carouselHead);

        // appends the parent container to the HTML body 
        body.append(parentContainer);
    }

    // generates a collection of cards or a single card based
    // on the cardinality property of the options.
    generateCard(cards) {
        let cardsToReturn = []

        if (cards !== undefined)
            cards.forEach(card => {
                let cardNode;

                if (card.cardinality === "single") {
                    cardNode = document.createElement("li");
                    cardNode.classList.add("card")
                    cardNode.innerHTML = `
                        <div class="image-container">
                            <img src="${card.image}" alt="">
                            <div class="image-container__footer">
                                <span class="argument">${card.type}</span>
                                <span class="duration">${this.formatTime(card.duration)}</span>
                            </div>
                        </div>
                        <div class="card-content">
                            <p class="card-title">${card.title}</p>
                            <p class="card-subtitle">${card.subtitle}</p>
                        </div>
                    `}
                else if (card.cardinality === "collection") {
                    cardNode = document.createElement("ul");
                    cardNode.classList.add("stack-cards")
                    cardNode.innerHTML = `
                        <li class="card">
                        </li>
                        <li class="card">
                        </li>
                        <li class="card">
                            <div class="image-container">
                                <img src="${card.image}" alt="">
                                <div class="image-container__footer">
                                    <span class="argument">${card.type}</span>
                                    <span class="duration">${this.formatTime(card.duration)}</span>
                                </div>
                            </div>
                            <div class="card-content">
                                <p class="card-title">${card.title}</p>
                                <p class="card-subtitle">${card.subtitle}</p>
                            </div>
                        </li>
                        `}

                cardsToReturn.push(cardNode)
            });

        return cardsToReturn
    }

    // formats a string (that needs to be rendered) 
    // by passing seconds from options.
    formatTime(secs) {
        let hours = Math.floor(secs / 3600);
        let minutes = Math.floor((secs - (hours * 3600)) / 60);
        let seconds = secs - (hours * 3600) - (minutes * 60);

        if (minutes < 10) { minutes = "0" + minutes; }
        if (seconds < 10) { seconds = "0" + seconds; }

        if (hours >= 1) return `${hours}h ${minutes}m`;
        else return minutes + ':' + seconds;
    }
}
